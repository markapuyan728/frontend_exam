# README #

Burst SMS Frontend Exam

### What is this repository for? ###

The repository aims to showcase the owner's skills in terms of frontend development. To materialize this project,
The owner made use of web technologies to answer the given challenge.


### Frontend Technologies Used ###

Frontend Technologies used are as follows:

HTML5 / CSS3
JavaScript
jQuery
SASS
Bootstrap Framework
AngularJS


### Running the App ###

The file should be put in a web server (localhost) to run.

### Who do I talk to? ###

Should there be any concern regarding this repo, The owner is more than willing to get in touch.