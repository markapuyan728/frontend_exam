var app = angular.module('mainApp', []);
app.controller('messageController', ['$scope', '$http', '$location', '$timeout', function($scope, $http, $location , $timeout){
    //getList of number codes
    $http({
        method : "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        url : "api/countryCode.json"
    }).then(function mySuccess(response) {
        $scope.codes = response.data.countries;
    }, function myError(response) {
        
    });
    //initialize
    $scope.selectedCode = + 63;
    $scope.selectedCountry = "ph";

    $scope.selected = function(code, country){
        $scope.selectedCode = code;
        $scope.selectedCountry = country;
    }
    $scope.showConfirmModal = false;

    $scope.processing = false;
    $scope.success = true;
    $scope.newMessage = true;
    $scope.sendText = '...';

    //create copy of original values for variables;
    $scope.oSelectedCode = $scope.selectedCode;
    $scope.oSelectedCountry = $scope.selectedCountry;
    $scope.oProcessing = $scope.processing;
    $scope.oSuccess = $scope.success;
    $scope.oNewMessage = $scope.newMessage;
    $scope.oSendText = $scope.sendText;

    //check if proceed check
    $scope.checkSend = function(){
        $scope.showConfirmModal =  !$scope.showConfirmModal;
    };

    //send message
    $scope.send = function(){
       $scope.showModal = !$scope.showModal;
       $scope.showConfirmModal = false;
        $timeout(function() {
            $scope.processing = true;
            $scope.success = false;
            $scope.newMessage = false;
            $scope.sendText = 'DONE';
      }, 2000);
    }
    //check if cancel
    $scope.cancel = function(){
      $scope.showConfirmModal = false;
    }

    //reset form
    $scope.resetForm = function(){
        $scope.phoneNumber = '';
        $scope.phoneMessage = '';
        $scope.smsForm.$setPristine(true);
        $scope.smsForm.$setUntouched(true);
        $scope.showModal = false;
        $scope.processing = $scope.oProcessing;
        $scope.success =  $scope.oSuccess;
        $scope.newMessage =  $scope.oNewMessage;
        $scope.sendText = $scope.oSendText;
        $scope.selectedCode = $scope.oSelectedCode;
        $scope.selectedCountry = $scope.oSelectedCountry;
    }
}])

//opens modal
app.directive('modal', function () {
    return {
      templateUrl: 'assets/template/modal.html',
      restrict: 'E',
      transclude: true,
      replace: true,
      scope: true,
      link: function postLink(scope, element, attrs) {
        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });


