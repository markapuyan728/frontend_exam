<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Burst SMS | Frontend Exam</title>
      <link rel="stylesheet" type="text/css" href="assets/third_party/bootstrap-3.3.7-dist/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="assets/css/style.css">
      <link rel="icon" href="favicon.ico" type="image/x-icon">
   </head>
   <body ng-app='mainApp'>
      <div class="container" ng-controller="messageController">
         <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4 col-center">
			<h1><img class="logo" src="assets/img/email.svg" alt="emailogo"> Burst SMS </h1>
               <form novalidate name="smsForm">
                  <div class="form-group">
                     <label for="phonenumber">Phone Number</label>
                     <div class="input-group">
                        <div class="input-group-btn">
                           <button class="btn btn-default dropdown-toggle phoneCodeList" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              <div class="flag {{ selectedCountry }}"></div>
                              +{{ selectedCode }}
                              <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li ng-repeat="code in codes"  ng-click="selected(code.codeno, code.cca2)">
                                 <a href="#" data-value="asd">
                                    <div class="flag {{ code.cca2 }}"></div>
                                    +{{ code.codeno }}
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <span ng-class="{'validated' : smsForm.phoneno.$valid , 'error' : smsForm.phoneno.$touched && smsForm.phoneno.$invalid}" ng-show="smsForm.phoneno.$touched" >
                        <i ng-class="{'glyphicon glyphicon-ok-sign' : smsForm.phoneno.$valid , 'glyphicon glyphicon-remove-sign' : smsForm.phoneno.$touched && smsForm.phoneno.$invalid}"> </i>
                        </span>
                        <input class="form-control" type="text" maxlength="10" ng-model="phoneNumber" name="phoneno"  required>
                     </div>
                     <p class="error-message" ng-show="smsForm.phoneno.$touched && smsForm.phoneno.$invalid">Invalid phone number</p>
                  </div>
                  <div class="form-group">
                     <div class="div-message">
                        <label for="message">Message</label>
                        <span class="divmessage" ng-class="{'validated' : smsForm.message.$valid , 'error' : smsForm.message.$touched && smsForm.message.$invalid}" ng-show="smsForm.message.$touched" >
                        <i ng-class="{'glyphicon glyphicon-ok-sign' : smsForm.message.$valid , 'glyphicon glyphicon-remove-sign' : smsForm.message.$touched && smsForm.message.$invalid}"> </i>
                        </span>
                        <textarea class="form-control"  rows="10" maxlength="420" ng-model="phoneMessage" name="message" ng-trim="false" required></textarea>
                     </div>
                     <p class="error-message" ng-show="smsForm.message.$touched && smsForm.message.$invalid">Invalid message</p>
                     <p class="counter">{{ 420 - phoneMessage.length }} / 420</p>
                  </div>
                  <input class="btn btn-default btn-block" type="submit" ng-click="checkSend()" ng-disabled="smsForm.$invalid "value="SEND"/>
               </form>
            </div>
         </div>
         <modal visible="showConfirmModal" id="confirmModal" data-backdrop="static">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="btn-group resultDiv">
                        <i class="glyphicon glyphicon-info-sign resultIcon"></i>
                        <h3 class="resultMessage">Are you sure you want to send message?</h3>
                        <button class="btn btn-default col-xs-6 col-sm-6" ng-click="send()">OK</button>
                        <button class="btn btn-default btn-cancel col-xs-6 col-sm-6" ng-click="cancel()">CANCEL</button>
                    </div>
                </div>             
            </div>
         </modal>    
         <modal visible="showModal" id="resultModal" data-backdrop="static">
            <div class="row">
                <div class="col-sm-12">
                    <div ng-hide="processing" class="resultDiv">
                        <i class="glyphicon glyphicon-send resultIcon"></i>
                        <h3 class="resultMessage">Sending Message...</h3>
                    </div>
                    <div ng-hide="success" class="resultDiv">
                        <i class="glyphicon glyphicon-ok resultIcon"></i>
                        <h3 class="resultMessage">Message Sent!</h3>
                    </div>
                    <input class="btn btn-default btn-block" type="submit" ng-disabled="newMessage" ng-click="resetForm()" value="{{ sendText }}"/>
                </div>             
            </div>
         </modal>
      </div>
      <script src="assets/js/jquery-1.12.4.min.js"></script>
      <script src="assets/third_party/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
      <script src="assets/js/angular.min.js"></script>
      <script src="assets/js/style.js"></script>
      <script src="assets/js/main.js"></script>
   </body>
</html>